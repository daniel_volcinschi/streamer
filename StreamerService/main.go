package main

import (
    "bitbucket.org/daniel_volcinschi/streamer/StreamerService/service"
    "log"
    "os"
)


func main(){
    port := os.Getenv("PORT")

    if port == ""{
        log.Fatal("missing $PORT")
    }

    streamerserver.StartWebServer(port)
}
