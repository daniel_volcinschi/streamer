package streamerserver

import (
    "bitbucket.org/daniel_volcinschi/streamer/model"
    "fmt"
    "github.com/golang/protobuf/proto"
    "github.com/twinj/uuid"
    "log"
    "strconv"
    "strings"
)

const MissingArgument int = 123

type ModelError struct {
    code    int
    message string
}

func (e *ModelError) Error() string {
    return fmt.Sprintf("%d - %s", e.code, e.message)
}

func createNumberSequenceFromRequest(requestVariables map[string]string) (*Sequence.NumberSequence, error) {

    numberSequence, ok := requestVariables["numberSequence"]
    if ok != true {
        return nil, &ModelError{code: MissingArgument, message: "missing numberSequence"}
    }

    clientId, ok := requestVariables["clientId"]
    if ok != true {
        return nil, &ModelError{code: MissingArgument, message: "missing clientId"}
    }

    separatedSequence := strings.Split(numberSequence, ",")
    numberArray := make([]int32, len(separatedSequence))
    for i, v := range separatedSequence {
        number, err := strconv.ParseInt(v, 10, 32)
        if err != nil {
            return nil, err
        }
        numberArray[i] = int32(number)
    }

    operationId := uuid.NewV4()

    result := &Sequence.NumberSequence{
        ClientId:    clientId,
        OperationId: operationId.String(),
        Sequence:    numberArray,
    }

    return result, nil
}

func createSequenceResultFromKafkaEvent(rawContent []byte) (*Sequence.NumberSequenceResult, error) {
    sequenceOperationResult := &Sequence.NumberSequenceResult{}
    err := proto.Unmarshal(rawContent, sequenceOperationResult)
    if err != nil {
        log.Println("Error: failed to get sequence content")
        log.Println("Error: " + err.Error())
        return nil, err
    }

    return sequenceOperationResult, nil
}
