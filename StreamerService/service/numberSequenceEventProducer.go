package streamerserver

import (
    "bitbucket.org/daniel_volcinschi/streamer/model"
    "github.com/Shopify/sarama"
    "github.com/golang/protobuf/proto"
    "log"
    "os"
)

func getKafkaHost() string {
    kafkaHost := os.Getenv("kafkaHost")
    if kafkaHost == "" {
        kafkaHost = "localhost:9092"
    }

    return kafkaHost
}

func getKafkaProducer() (sarama.SyncProducer, error) {
    config := sarama.NewConfig()
    config.ClientID = "go-kafka-consumer"
    config.Consumer.Return.Errors = true
    config.Producer.Return.Successes = true

    brokers := []string{getKafkaHost()}

    return sarama.NewSyncProducer(brokers, config)
}

func produceEvent(model *Sequence.NumberSequence) error {

    producer, err := getKafkaProducer()
    if err != nil {
        log.Println("Error: Kafka failed to initialize")
        log.Println("Error: " + err.Error())
        return err
    }

    marshaledModel, err := proto.Marshal(model)
    if err != nil {
        return err
    }

    message := &sarama.ProducerMessage{
        Topic:     "numberSequence",
        Partition: 0,
        Value:     sarama.ByteEncoder(marshaledModel),
    }

    _, _, err = producer.SendMessage(message)
    if err != nil {
        log.Println("Error: failed to post to Kafka")
        log.Println("Error: " + err.Error())
        return err
    }

    return nil
}
