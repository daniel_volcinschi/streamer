package streamerserver

import (
    "github.com/gorilla/mux"
    "log"
    "net/http"
)

type Route struct {
    Name        string
    Method      string
    Pattern     string
    HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{

    Route{
        "AddNumber",
        "POST",
        "/numberSequence/{numberSequence}/clientId/{clientId}",
        func(writer http.ResponseWriter, request *http.Request) {
            handleNumberSequence(writer, request, false)
        },
    },
    Route{
        "AddNumberFail",
        "POST",
        "/numberSequenceFail/{numberSequence}/clientId/{clientId}",
        func(writer http.ResponseWriter, request *http.Request) {
            handleNumberSequence(writer, request, true)
        },
    },
    Route{
        "RetrieveOperation",
        "GET",
        "/operation/{operationId}",
        func(writer http.ResponseWriter, request *http.Request) {
            handleRetrieveOperation(writer, request)
        },
    },
}

func NewRouter() *mux.Router {

    router := mux.NewRouter().StrictSlash(true)

    for _, route := range routes {

        router.Methods(route.Method).
            Path(route.Pattern).
            Name(route.Name).
            Handler(route.HandlerFunc)
    }
    return router
}

func StartWebServer(port string) {

    r := NewRouter()
    http.Handle("/", r)

    log.Println("Starting HTTP service at " + port)
    err := http.ListenAndServe(":" + port, nil)

    if err != nil {
        log.Println("An error occurred starting HTTP listener at port " + port)
        log.Println("Error: " + err.Error())
    }
}
