package streamerserver

import (
    "bitbucket.org/daniel_volcinschi/streamer/model"
    "encoding/json"
    "fmt"
    "github.com/gorilla/mux"
    "log"
    "net/http"
)

const SimulatedFailure int = 124

type ControllerError struct {
    code    int
    message string
}

func (e *ControllerError) Error() string {
    return fmt.Sprintf("%d - %s", e.code, e.message)
}

func handleNumberSequence(writer http.ResponseWriter, request *http.Request, simulateFailure bool) {
    operationId, err := handleAddSequence(writer, request, simulateFailure)
    if err != nil {
        switch err.(type) {
        default:
            writer.WriteHeader(http.StatusInternalServerError)
        case *ModelError:
            writer.WriteHeader(http.StatusBadRequest)
        case *ControllerError:
            jsonOperationId, errJ := json.Marshal(operationId)
            if errJ != nil {
                writer.WriteHeader(http.StatusInternalServerError)
            } else {
                _, errW := writer.Write(jsonOperationId)
                if errW != nil {
                    writer.WriteHeader(http.StatusInternalServerError)
                } else {
                    // simulation error
                    writer.WriteHeader(http.StatusServiceUnavailable)
                }
            }
        }
    } else {
        writer.WriteHeader(http.StatusOK)
    }
}

func handleAddSequence(writer http.ResponseWriter, request *http.Request, simulateFailure bool) (string, error) {
    writer.Header().Set("Content-Type", "application/json; charset=UTF-8")

    vars := mux.Vars(request)

    model, err := createNumberSequenceFromRequest(vars)
    if err != nil {
        return "", err
    }

    doneCh := make(chan *Sequence.NumberSequenceResult)

    // check if simulating a service failure
    if simulateFailure != true {
        go func() {
            result, errC := getSequenceOperationResult(model.OperationId, false)
            if errC != nil {
                log.Println("Error: failed to get NumberSequenceResult");
                log.Println("Error: " + errC.Error())
            }
            doneCh <- result
        }()
    }

    errS := produceEvent(model)
    if errS != nil {
        return "", errS
    }

    if simulateFailure != true {
        operationResult := <-doneCh

        jsonResult, errM := json.Marshal(operationResult.LongestSequence)
        if errM != nil {
            log.Println("Error: failed to marshal NumberSequenceResult");
            log.Println("Error: " + errM.Error())
            return "", errM
        }

        _, errW := writer.Write(jsonResult)
        if errW != nil {
            log.Println("Error: failed to write NumberSequenceResult");
            log.Println("Error: " + errW.Error())
            return "", errW
        }
    } else {
        return model.OperationId, &ControllerError{code: SimulatedFailure, message: "Simulated failure"}
    }

    return model.OperationId, nil
}

func handleRetrieveOperation(writer http.ResponseWriter, request *http.Request) {
    vars := mux.Vars(request)
    operationId := vars["operationId"]
    doneCh := make(chan *Sequence.NumberSequenceResult)
    go func() {
        result, errC := getSequenceOperationResult(operationId, true)
        if errC != nil {
            log.Println("Error: failed to get NumberSequenceResult");
            log.Println("Error: " + errC.Error())
        }
        doneCh <- result
    }()

    operationResult := <-doneCh

    sequenceJson, errJ := json.Marshal(operationResult.LongestSequence)
    if errJ != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        return
    }

    _, errW := writer.Write(sequenceJson)
    if errW != nil {
        writer.WriteHeader(http.StatusInternalServerError)
        return
    }

    writer.WriteHeader(http.StatusOK)
}
