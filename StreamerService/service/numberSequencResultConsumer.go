package streamerserver

import (
    "bitbucket.org/daniel_volcinschi/streamer/model"
    "github.com/Shopify/sarama"
    "log"
    "os"
    "os/signal"
)

func getKafkaConsumer() (sarama.Consumer, error) {
    config := sarama.NewConfig()
    config.Consumer.Return.Errors = true
    config.ClientID = "number-streamer-service"
    brokers := []string{getKafkaHost()}

    master, err := sarama.NewConsumer(brokers, config)
    if err != nil {
        log.Fatal("Error: failed to create Kafka Consumer")
        log.Fatal("Error: " + err.Error())
        return nil, err
    }

    return master, nil
}

func getSequenceOperationResult(operationId string, fromBeginning bool) (*Sequence.NumberSequenceResult, error) {

    master, err := getKafkaConsumer()
    if err != nil {
        return nil, err
    }

    defer func() {
        if err := master.Close(); err != nil {
            panic(err)
        }
    }()

    partitionScanOffset := sarama.OffsetNewest
    if fromBeginning {
        partitionScanOffset = sarama.OffsetOldest
    }

    topic := "numberSequenceResult"
    consumer, errConsumer := master.ConsumePartition(topic, 0, partitionScanOffset)
    if errConsumer != nil {
        log.Println("Error: failed to subscribe to Kafka")
        log.Println("Error: " + errConsumer.Error())
        return nil, errConsumer
    }

    signals := make(chan os.Signal, 1)
    signal.Notify(signals, os.Interrupt)

    sequenceOperationResult := &Sequence.NumberSequenceResult{}

    running := true
    for running {
        select {
        case err := <-consumer.Errors():
            log.Println("Error: " + err.Error())
            running = false;
        case msg := <-consumer.Messages():
            if string(msg.Key) == operationId {
                operationResult, _ := createSequenceResultFromKafkaEvent(msg.Value)
                sequenceOperationResult = operationResult
                running = false
            }
        case <-signals:
            log.Println("Interrupt is detected")
            running = false
        }
    }

    return sequenceOperationResult, nil
}
