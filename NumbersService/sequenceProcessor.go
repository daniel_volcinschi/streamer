package main

import (
    "bitbucket.org/daniel_volcinschi/streamer/model"
    "github.com/Shopify/sarama"
    "github.com/golang/protobuf/proto"
    "github.com/samuel/go-zookeeper/zk"
    "log"
    "os"
    "time"
)

func getKafkaHost() string {
    kafkaHost := os.Getenv("kafkaHost")
    if kafkaHost == "" {
        kafkaHost = "localhost:9092"
    }

    return kafkaHost
}

func getZookeeperHot() string {
    zookeeper := os.Getenv("ZookeeperHost")
    if zookeeper == "" {
        zookeeper = "localhost"
    }

    return zookeeper
}

func shouldUseNoncontinuousSequenceProcessing() bool {

    connection, _, err := zk.Connect([]string{getZookeeperHot()}, time.Second) //*10)
    if err != nil {
        log.Println("Error: failed to connect to zookeeper")
        log.Println("Error: " + err.Error())
        return false
    }

    rawMethod, _, errG := connection.Get("/numberSequenceMethod")
    if errG != nil {
        log.Println("Error : failed to retrieve sequence dominant member processing method")
        log.Println("Error: " + err.Error())
        return false
    }

    numberSequenceMethod := string(rawMethod)

    // If value is not set or set to anything else other than 'noncontinuous' processing will be done on sub-arrays.
    return numberSequenceMethod == "noncontinuous"
}

func getKafkaProducer() (sarama.SyncProducer, error) {
    config := sarama.NewConfig()
    config.ClientID = "go-kafka-consumer"
    config.Consumer.Return.Errors = true
    config.Producer.Return.Successes = true

    brokers := []string{getKafkaHost()}

    return sarama.NewSyncProducer(brokers, config)
}

func produceResponse(operationId string, sequenceLength int, sequenceElementType int32) {

    producer, err := getKafkaProducer()
    if err != nil {
        log.Println("Error: Kafka failed to initialize")
        log.Println("Error: " + err.Error())
        return
    }

    result := make([]int32, sequenceLength)
    for i := range result {
        result[i] = sequenceElementType
    }

    model := &Sequence.NumberSequenceResult{
        OperationId:     operationId,
        LongestSequence: result,
    }

    marshaledModel, err := proto.Marshal(model)
    if err != nil {
        return
    }

    message := &sarama.ProducerMessage{
        Topic:     "numberSequenceResult",
        Partition: 0,
        Value:     sarama.ByteEncoder(marshaledModel),
        Key : sarama.StringEncoder(operationId),
    }

    _, _, err = producer.SendMessage(message)
    if err != nil {
        log.Println("Error: failed to post to Kafka")
        log.Println("Error: " + err.Error())
        return
    }
}

// Computes the dominant element in a continuous sequence (i.e. the longest continuous sequence)
func computeLongestContinuousSequence(numberSequence *Sequence.NumberSequence) {

    maxLen := -1
    longestSequenceElement := int32(0)

    if len(numberSequence.Sequence) > 0 {

        prev := numberSequence.Sequence[0]
        sequenceLength := 1
        for _, v := range numberSequence.Sequence[1:] {
            if prev != v {
                prev = v
                sequenceLength = 1
            } else {
                sequenceLength = sequenceLength + 1
            }

            if maxLen < sequenceLength {
                maxLen = sequenceLength
                longestSequenceElement = v
            }
        }
    }

    produceResponse(numberSequence.OperationId, maxLen, longestSequenceElement)
}

// Computes the dominant element in a noncontinuous sequence
func computeLongestNoncontinuousSequence(numberSequence *Sequence.NumberSequence) {
    appearance := 0
    candidate := int32(-1)
    for _, v := range numberSequence.Sequence {
        if appearance == 0 {
            candidate = v
            appearance = 1
        } else if candidate == v {
            appearance = appearance + 1
        } else {
            appearance = appearance - 1
        }
    }

    dominantElementAppearance := 0
    for _, v := range numberSequence.Sequence {
        if v == candidate {
            dominantElementAppearance = dominantElementAppearance + 1
        }
    }

    produceResponse(numberSequence.OperationId, dominantElementAppearance, candidate)
}

func processMessage(content []byte) {
    sequence := &Sequence.NumberSequence{}
    err := proto.Unmarshal(content, sequence)
    if err != nil {
        log.Println("Error: failed to get sequence content")
        log.Println("Error: " + err.Error())
        return
    }

    if shouldUseNoncontinuousSequenceProcessing() {
        computeLongestNoncontinuousSequence(sequence)
    } else {
        computeLongestContinuousSequence(sequence)
    }
}
