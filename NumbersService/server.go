package main

import (
    "github.com/Shopify/sarama"
    "log"
    "os"
    "os/signal"
)

func RunServer() {

    config := sarama.NewConfig()
    config.ClientID = "number-sequence-processor"
    config.Consumer.Return.Errors = true

    brokers := []string{getKafkaHost()}

    master, err := sarama.NewConsumer(brokers, config)
    if err != nil {
        panic(err)
    }

    defer func() {
        if err := master.Close(); err != nil {
            panic(err)
        }
    }()

    topic := "numberSequence"
    consumer, err := master.ConsumePartition(topic, 0, sarama.OffsetNewest)
    if err != nil {
        panic(err)
    }

    signals := make(chan os.Signal, 1)
    signal.Notify(signals, os.Interrupt)

    doneCh := make(chan struct{})
    go func() {
        for {
            select {
            case err := <-consumer.Errors():
                log.Println(err)
            case msg := <-consumer.Messages():
                processMessage(msg.Value)
            case <-signals:
                log.Println("Interrupt is detected")
                doneCh <- struct{}{}
            }
        }
    }()

    <-doneCh
}
