request:

Create a RESTFul Service in GoLang which will take a string of integers and find the longest running sequence if elements.
For example 1,1,2,2,2 will give 222 as the longest sequence.
The service needs to be deployed in Azure and can be demoable.





architecture:

The system has 3 parts : (1) front facing REST service that uses a (2) Kafka cluster to broker requests to (3) an internal processing service.
The reasoning was to simulate a non-trivial processing flow while using a fast and durable queing system to allow building a mechanism to reconstruct operations in the event of a failure.



### DataModel (model) ProtoBuf model for 
    1. request operations (process number sequence request)
    2. operations results


### API (StreamerService project)
    1. POST /numberSequence/{numberSequence}/clientId/{clientId}
       allow posting a sequence of integers for a client and returns the dominant sequence 
    2. POST /numberSequenceFail/{numberSequence}/clientId/{clientId}
       same like above - with the difference that this is a test hook to simulate an internal failure in the lower level
       Returns the operationId so that we could reconstruct the result whenever if becomes available.
    3. GET /operation/{operationId}
       retrieves, if available, the operation result


### Internal number processing service (NumberService)
    It's a simple Go executable acting as a daemon service - which consumes a kafka topic "numberSequence" and writes back operations to result topic "numberSequenceResult"
    I've also implemented a zookeeper control flow for how the "dominant sequence" is calculated to simulate a "bug" in code : this will allow to adjust the service operation without deploying code in production (we'd just have to set the Zookeeper /numberSequenceMethod. 
    There are 2 methods for calculating the dominant sequence
    1) computeLongestContinuousSequence : returns the "max subarray"
    2) computeLongestNoncontinuousSequence : returns the "dominant element" sequence
                            
